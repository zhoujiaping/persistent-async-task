package com.example.task.core;

import com.example.task.util.$;
import com.example.task.util.JSON;
import org.example.jdbc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;

import static org.example.jdbc.Utils.also;

@Repository
public class TaskRepo extends JdbcRepo<Task> {
    @Autowired
    public void setDs(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @PostConstruct
    public void init() {
        this.initializer = repo -> {
            entityClazz = Task.class;
            rowMapper = new BeanPropertyRowMapper(entityClazz);
            tableMeta = new TableMeta();
            tableMeta.setTableName("t_flow_task");
            tableMeta.setHistoryTableName(tableMeta.getTableName() + "_his");
            tableMeta.setPrimaryKeyColumnName("id");
            tableMeta.setPrimaryKeyFieldName("id");
            tableMeta.setVersionColumnName("version");
            tableMeta.setVersionFieldName("version");

            ReflectionUtils.doWithFields(entityClazz, field -> {
                //it.setAccessible(true);
                if (field.getAnnotation(Transient.class) != null) {
                    return;
                }
                String fieldName = field.getName();
                String columnName = Utils.camelNameToUnderscoreName(fieldName);
                tableMeta.addColumnMeta(also(new ColumnMeta(), columnMeta -> {
                    columnMeta.setColumnName(columnName);
                    columnMeta.setFieldName(fieldName);
                    columnMeta.setField(field);
                }));
            }, f -> !Modifier.isStatic(f.getModifiers()) && !Modifier.isFinal(f.getModifiers()));
        };
    }

    int updateToSuccess(Task task) {
        Integer execTimes = task.getExecTimes()+1;
        String extra = JSON.stringify(task.getVariables());
        Date now = new Date();
        String status = TaskStatus.SUCCESS.name();
        int count = updateSelectiveWithOptiLock($.also(new Task(), it -> {
            it.setId(task.getId());
            it.setVersion(task.getVersion());
            it.setExecTimes(execTimes);
            //it.setDelay(task.getDelay());
            it.setStatus(status);
            it.setExtra(extra);
            it.setArgs(task.getArgs());
            it.setUpdateTime(now);
        }));
        task.setExecTimes(execTimes);
        task.setExtra(extra);
        task.setUpdateTime(now);
        task.setStatus(status);
        task.setVersion(task.getVersion()+1);
        return count;
    }

    int updateToFailed(Task task) {
        Integer execTimes = task.getExecTimes()+1;
        String extra = JSON.stringify(task.getVariables());
        Date now = new Date();
        String status = TaskStatus.FAILED.name();
        int count = updateSelectiveWithOptiLock($.also(new Task(), it -> {
            it.setId(task.getId());
            it.setVersion(task.getVersion());
            it.setExecTimes(execTimes);
            //it.setDelay(task.getDelay());
            it.setStatus(status);
            it.setExtra(extra);
            it.setArgs(task.getArgs());
            it.setUpdateTime(now);
        }));
        task.setExecTimes(execTimes);
        task.setExtra(extra);
        task.setUpdateTime(now);
        task.setStatus(status);
        task.setVersion(task.getVersion()+1);
        return count;
    }

    Long insertTask(Task task) {
        Assert.notNull(task.getBeanName(), "beanName cannot be empty!");
        Assert.notNull(task.getMethod(), "method cannot be empty!");
        Date now = new Date();
        if (task.getUid() == null) {
            task.setUid(UUID.randomUUID().toString());
        }
        task.setExecTimes(0);
        //task.setDelay(0);
        task.setStatus(TaskStatus.PENDING.name());
        task.setExtra(JSON.stringify(task.getVariables()));
        task.setCreateTime(now);
        task.setUpdateTime(now);
        task.setScheduleTime(now);
        task.setExpectExecTime(Date.from(
                LocalDateTime.from(now.toInstant()).plusSeconds(task.getDelay())
                        .toInstant(ZoneOffset.UTC)
        ));
        task.setVersion(1);
        return insertSelective(task);
    }
}
