package com.example.task.core;

import com.example.task.util.$;
import lombok.extern.slf4j.Slf4j;
import org.example.jdbc.PageParam;
import org.example.jdbc.PageRes;
import org.example.jdbc.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@Component
@Slf4j
public class Polling {
    private TaskRepo taskRepo;
    private TaskExecutor taskExecutor;

    @Autowired
    public void setBeanTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Autowired
    public void setTaskRepo(TaskRepo taskRepo) {
        this.taskRepo = taskRepo;
    }

    /**
     * 补偿，处理超时处于PENDING、FAILED状态的任务
     */
    public void compensate() {
        Date expectExecTimeMax = Date.from(LocalDateTime.now().plusSeconds(-60).toInstant(ZoneOffset.UTC));
        // 轮询条件 schedule_time+delay+max_task_time < now()  max_task_time为任务最大执行时间，要设置为比业务的事务超时时间大一点。
        String sql = "select * from t_flow_task where id>:prevMaxId expect_exec_time<=:expectExecTimeMax and ((status = 'FAILED' and exec_times<3) or status = 'PENDING') order by id asc";
        PageRes<Task> pageRes;
        Long prevMaxId = -1L;
        do {
            //TODO 不需要分页，直接查即可
            pageRes = taskRepo.queryPagination(sql, Utils.mapOf("prevMaxId", prevMaxId, "expectExecTimeMax", expectExecTimeMax), $.also(new PageParam(), it -> {
                it.setPage(1);
                it.setSize(20);
            }), Task.class);
            pageRes.getRows().forEach(it -> {
                try {
                    taskExecutor.invoke(it);
                } catch (Exception e) {
                    log.error("invoke task error", e);
                }
            });
            if (!pageRes.getRows().isEmpty()) {
                prevMaxId = $.getAt(pageRes.getRows(), -1).getId();
            }
        } while (!pageRes.getRows().isEmpty());
    }
}
