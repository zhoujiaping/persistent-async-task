package com.example.task.core;

import java.io.Serializable;
import java.util.Map;

@FunctionalInterface
public interface TaskInvoker extends Serializable {
    void invoke(String args, Map<String,Object> extra);
}
