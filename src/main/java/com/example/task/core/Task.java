package com.example.task.core;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.example.jdbc.Transient;
import java.util.Date;
import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Task{
    Long id;
    String uid;
    String beanName;
    String method;
    String args;
    String extra;
    Integer version;
    Date createTime;
    Date updateTime;
    String status;
    Date scheduleTime;
    Date expectExecTime;
    Integer execTimes;
    Integer delay;

    @Transient
    Map<String,Object> variables;

}