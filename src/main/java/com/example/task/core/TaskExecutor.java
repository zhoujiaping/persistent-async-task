package com.example.task.core;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.example.task.TxHelper;
import com.example.task.anno.TaskBean;
import com.example.task.util.$;
import com.example.task.util.AppCtxUtil;
import com.example.task.util.LambdaUtil;
import lombok.Lombok;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**

 */
@Slf4j
@Component
public class TaskExecutor {
    protected static final TransmittableThreadLocal<Task> taskTl = new TransmittableThreadLocal<>();
    protected static final TransmittableThreadLocal<List<$.Fn00>> scheduleActionsTl = new TransmittableThreadLocal<>();

    private TaskRepo taskRepo;
    private TxHelper txHelper;
    private ScheduledThreadPoolExecutor threadPoolExecutor;

    @Autowired
    public void setTaskRepo(TaskRepo taskRepo) {
        this.taskRepo = taskRepo;
    }

    @Autowired
    public void setTxHelper(TxHelper txHelper) {
        this.txHelper = txHelper;
    }

    @Autowired
    public void setThreadPoolExecutor(ScheduledThreadPoolExecutor threadPoolExecutor) {
        this.threadPoolExecutor = threadPoolExecutor;
    }

    /**
     * 线程安全问题
     * a线程创建task对象，设置属性。
     * a线程再安排一个线程b。b线程里面也会修改task对象。
     * task对象是存在堆内存中的，多个线程修改它，会有线程安全问题。
     * 不过现在的场景，是a线程写->b线程读->b线程写。不会有线程安全问题。
     * a线程写完，b线程还没有缓存该数据，b线程读到线程缓存的时候，a线程不会再去读写。
     */
    @SneakyThrows
    public void async(Integer delaySeconds,TaskInvoker invoker,String args,Map<String,Object> variables) {
        Task task = new Task();
        task.setUid(UUID.randomUUID().toString());
        task.setDelay(delaySeconds);
        SerializedLambda lambda = LambdaUtil.getSerializedLambda(invoker);
        //eg: lambda.getImplClass() ==> com/example/credit/service/CreditFlow
        Class<?> lambdaImplClass = Class.forName(lambda.getImplClass().replace('/','.'));
        task.setBeanName(lambdaImplClass.getAnnotation(TaskBean.class).beanName());
        task.setMethod(lambda.getImplMethodName());
        task.setArgs(args);
        task.setVariables(variables);
        //新增任务，需要将其和上一个流程节点的业务逻辑放在一个事务中。
        taskRepo.insertTask(task);
        // 安排任务
        scheduleTask(task);
    }
    /**并不实际安排任务，而是先保存要安排的任务，等事务提交之后再执行任务安排。*/
    private void scheduleTask(Task task) {
        List<$.Fn00> scheduleActions = scheduleActionsTl.get();
        //非flowBean中调用
        if(scheduleActions == null){
            threadPoolExecutor.schedule(() -> invoke(task), task.getDelay(), TimeUnit.SECONDS);
        }else{
            //flowBean中调用
            scheduleActions.add(()->
                    threadPoolExecutor.schedule(() -> invoke(task), task.getDelay(), TimeUnit.SECONDS)
            );
        }
    }

    @SneakyThrows
    public void invoke(Task task) {
        Assert.isNull(taskTl.get(),"task already exists!");
        Assert.isNull(scheduleActionsTl.get(),"scheduleActions already exists!");
        Throwable err = null;
        try{
            taskTl.set(task);
            List<$.Fn00> scheduleActions = new ArrayList<>();
            scheduleActionsTl.set(scheduleActions);
            try {
                //单独一个事务执行业务方法
                Object bean = AppCtxUtil.getBean(task.getBeanName());
                Method method = ReflectionUtils.findMethod(bean.getClass(), task.getMethod(),String.class,Map.class);
                ReflectionUtils.invokeMethod(method,bean,task.getArgs(),task.getVariables());
            } catch (Throwable e) {
                err = e;
            }
            if (err == null) {
                //更新当前任务为success
                txHelper.withNewTx(() -> taskRepo.updateToSuccess(task));
                scheduleActions.forEach(it->it.apply());
            } else {
                //更新当前任务为failed
                txHelper.withNewTx(() -> taskRepo.updateToFailed(task));
                throw Lombok.sneakyThrow(err);
            }
        }finally {
            taskTl.remove();
            scheduleActionsTl.remove();
        }
    }
    @SneakyThrows
    public void retryCurrentTask() {
        Task task = taskTl.get();
        scheduleTask(task);
    }
}
