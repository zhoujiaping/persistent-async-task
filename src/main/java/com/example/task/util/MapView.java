package com.example.task.util;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.Date;
import java.util.Map;

public class MapView {
    private ConversionService conversionService = new DefaultConversionService();
    private Map<String, Object> map;

    public static MapView of(Map<String,Object> map){
        return new MapView(map);
    }
    private MapView(Map<String, Object> map) {
        this.map = map;
    }

    public Object get(String key) {
        return map.get(key);
    }
    /*public <T> T getOrDefault(String key,T defaultValue){
        Object value = map.get(key);
        if(value == null){
            return defaultValue;
        }
        return (T) value;
    }*/
    public <T> T getOrDefault(String key,T defaultValue,Class<T> clazz){
        Object value = map.get(key);
        if(value == null){
            return defaultValue;
        }
        return conversionService.convert(value,clazz);
    }

    public <T> T get(String key, Class<T> clazz) {
        return conversionService.convert(get(key), clazz);
    }

    public Integer getInteger(String key) {
        return get(key, Integer.class);
    }
    public Integer getIntegerOrDefault(String key,Integer defaultValue) {
        return getOrDefault(key,defaultValue,Integer.class);
    }

    public String getString(String key) {
        return get(key, String.class);
    }

    public Long getLong(String key) {
        return get(key, Long.class);
    }

    public Date getDate(String key) {
        return get(key, Date.class);
    }

    public Boolean getBoolean(String key) {
        return get(key, Boolean.class);
    }
}
