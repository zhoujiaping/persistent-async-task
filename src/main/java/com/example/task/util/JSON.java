package com.example.task.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class JSON {
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS").create();
    public static String stringify(Object obj){
        return gson.toJson(obj);
    }
    public static <T> T parse(String json,Class<T> clazz){
        return gson.fromJson(json,clazz);
    }
}