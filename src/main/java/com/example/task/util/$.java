package com.example.task.util;

import java.util.List;
import java.util.function.Consumer;

public abstract class $ {
    public static <T> T also(T t, Consumer<T> fn){
        fn.accept(t);
        return t;
    }
    public interface Fn00{
        void apply();
    }
    public static <T> T getAt(List<T> list, int index){
        if(index<0){
            index = list.size()+index;
        }
        return list.get(index);
    }
}
