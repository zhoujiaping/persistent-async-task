package com.example.task.util;

import lombok.SneakyThrows;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author stan
 */
public abstract class LambdaUtil {
    private static final Map<Class, SerializedLambda> CLASS_LAMBDA_CACHE = new ConcurrentHashMap<>();

    @SneakyThrows
    public static SerializedLambda getSerializedLambda(Serializable fn) {
        SerializedLambda lambda = CLASS_LAMBDA_CACHE.get(fn.getClass());
        if (lambda == null) {
            Method method = fn.getClass().getDeclaredMethod("writeReplace");
            method.setAccessible(true);
            lambda = (SerializedLambda) method.invoke(fn);
            CLASS_LAMBDA_CACHE.put(fn.getClass(), lambda);
        }
        return lambda;
    }
}
