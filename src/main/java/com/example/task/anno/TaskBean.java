package com.example.task.anno;

import java.lang.annotation.*;

/**
 * @author
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface TaskBean {
    String beanName();
}
