package com.example.task;

import com.alibaba.druid.pool.DruidDataSource;
import com.example.task.util.NamedThreadFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class Config {
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return new DruidDataSource();
    }

    @Bean
    public ScheduledThreadPoolExecutor threadPoolExecutor() {
        return new ScheduledThreadPoolExecutor(10,
                new NamedThreadFactory("task"),
                new ThreadPoolExecutor.AbortPolicy());
    }
}
