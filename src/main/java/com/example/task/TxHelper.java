package com.example.task;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;

@Component
public class TxHelper {
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public <T> T withTx(Supplier<T> fn){
        return fn.get();
    }
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRES_NEW)
    public <T> T withNewTx(Supplier<T> fn){
        return fn.get();
    }
}
