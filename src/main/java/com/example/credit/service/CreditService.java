package com.example.credit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class CreditService {
    @Transactional(rollbackFor = Exception.class)
    public Object saveApplyRequest(String req){
        log.info("保存授信申请");
        return "applied";
    }

    public void check(String args) {
        log.info("基本准入校验");
    }

    public void uploadImg(String args) {
        log.info("营业执照上传到ftp");
    }

    public void signSeal(String args) {
        log.info("合同签章");
    }

    public void riskCtl(String args) {
        log.info("风控调用");
    }
}
