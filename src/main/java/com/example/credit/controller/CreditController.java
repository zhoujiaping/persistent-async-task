package com.example.credit.controller;

import com.example.credit.req.CreditApplyReq;
import com.example.credit.flow.CreditFlow;
import com.example.task.core.TaskExecutor;
import com.example.task.util.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/credit")
public class CreditController {
    @Autowired
    CreditFlow creditFlow;
    @Autowired
    TaskExecutor taskExecutor;
    @RequestMapping("/apply")
    public Object apply(CreditApplyReq applyReq){
        //调用流程的方法，都通过beanTaskExecutor来调。不然某些情况下某些功能用不了，比如入口方法无法调用beanTaskExecutor#retryCurrentTask。
        taskExecutor.async(0,creditFlow::apply,JSON.stringify(applyReq),new HashMap<>());
        return "授信申请已受理";
    }
}
