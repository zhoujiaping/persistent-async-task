package com.example.credit.flow;

import com.example.credit.service.CreditService;
import com.example.task.anno.TaskBean;
import com.example.task.core.TaskExecutor;
import com.example.task.util.MapView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 多线程场景spring ioc字段存在问题，在异步任务中取不到注入的对象。
 */
@Component
@TaskBean(beanName = "creditFlow")
@Slf4j
public class CreditFlow {

    TaskExecutor taskExecutor;
    CreditService creditService;

    @Autowired
    public void setBeanTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Autowired
    public void setCreditService(CreditService creditService) {
        this.creditService = creditService;
    }

    /**
     * 授信流程入口
     */
    @Transactional(rollbackFor = Exception.class)
    public void apply(String args, Map<String, Object> variables) {
        //委托为授信服务，保存授信请求
        creditService.saveApplyRequest(args);
        //流程任务执行器执行授信流程的“基本准入校验”任务。
        //problem:事务还没提交，异步任务已经执行，这时候异步任务读取的数据不正确。
        //解决方案，拦截方法，在方法执行完，事务提交完成之后再实际安排异步任务。
        taskExecutor.async(1,this::check,args,variables);
    }
    /**
     “基本准入校验”任务。
     */
    @Transactional(rollbackFor = Exception.class)
    public void check(String args, Map<String, Object> variables) {
        //委托给授信服务，基本准入校验
        creditService.check(args);
        //流程任务执行器执行授信流程的“图片上传”任务。
        taskExecutor.async(1,this::uploadImg,args,variables);
    }

    @Transactional(rollbackFor = Exception.class)
    public void uploadImg(String args, Map<String, Object> variables) {
        creditService.uploadImg(args);
        taskExecutor.async(1,this::signSeal,args,variables);
    }

    AtomicInteger signSealCallCount = new AtomicInteger(0);
    @Transactional(rollbackFor = Exception.class)
    public void signSeal(String args, Map<String, Object> variables) {
        log.info("===signSeal");
        try {
            //模拟执行失败的情况，失败两次
            if (signSealCallCount.getAndIncrement()<2) {
                throw new RuntimeException();
            }
            creditService.signSeal(args);
            taskExecutor.async(1,this::riskCtl, args, variables);
        }catch (Exception err){
            Integer errTimes = MapView.of(variables).getIntegerOrDefault("errTimes", 0);
            //失败3次，则直接抛异常，否则安排重试之后再抛异常。
            if (errTimes >= 2) {
                throw err;
            } else {
                //log.error("signSealError", err);
                variables.put("errTimes", errTimes + 1);
                //重试当前任务
                taskExecutor.retryCurrentTask();
                throw err;
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void riskCtl(String args, Map<String, Object> variables) {
        creditService.riskCtl(args);
    }
}
