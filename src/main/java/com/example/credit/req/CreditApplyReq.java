package com.example.credit.req;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreditApplyReq {
    String user;
    BigDecimal amt;
}
