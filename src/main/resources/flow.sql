-- drop table t_flow_task;
create table t_flow_task(
    id bigint(20) primary key auto_increment,
    uid varchar(36) comment '任务的唯一标识',
    version int default 0 not null comment '乐观锁版本号',
    create_time datetime default now() comment '创建时间',
    update_time datetime default now() comment '更新时间',
    status varchar(40) not null comment '任务状态。PENDING,SUCCESS,FAILED',
    schedule_time datetime default now() comment '任务被安排时的时间',
    delay int default 0 comment '延迟时间，单位（秒）',
    expect_exec_time datetime not null comment '预期执行时间',
    exec_times int default 0 comment '任务执行次数',
    -- 轮询条件 schedule_time+delay+max_task_time < now()  max_task_time为任务最大执行时间，要设置为比业务的事务超时时间大一点。
    bean_name varchar(128) comment '流程对应的bean',
    method varchar(128) comment '任务对应的bean方法',
    args varchar(5000) comment '任务的业务参数，json格式',
    extra varchar(2000) comment '任务的控制参数，json格式'
) comment '流程任务';
-- drop table t_flow_task_his;
create table t_flow_task_his(
    id bigint(20) primary key,
    uid varchar(36) comment '任务的唯一标识',
    version int,
    create_time datetime,
    update_time datetime,
    status varchar(40) not null comment '任务状态。PENDING,SUCCESS,FAILED',
    schedule_time datetime comment '任务被安排时的时间',
    delay int comment '延迟时间，单位（秒）',
    exec_times int comment '任务执行次数',
    bean_name varchar(128) comment '流程对应的bean',
    method varchar(128) comment '任务对应的bean方法',
    args varchar(5000) comment '任务的业务参数，json格式',
    extra varchar(2000) comment '任务的控制参数，json格式'
) comment '流程任务历史表';