import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

public class ConsumerTest {
    @Test
    public void test(){
        String data = "test data";
        Consumer<String> insertAction = it->System.out.println("insert "+it);
        //insert(data,insertAction.andThen(System.out::println));
        //insert(data,insertAction);
        insertAction.accept(data);
    }
//    private void insert(String data, Consumer<String> insertAction){
//        insertAction.accept(data);
//    }
}
